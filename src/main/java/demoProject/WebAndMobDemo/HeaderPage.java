package demoProject.WebAndMobDemo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderPage extends BasePage {

	public HeaderPage(WebDriver driver) {
		// this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(id = "contact-link")
	WebElement contactUs;
	
	public void clickContactLink() {
		contactUs.click();
	}
	
	public String getContactUsURL() {
		return driver.getCurrentUrl().toString();
	}
}
