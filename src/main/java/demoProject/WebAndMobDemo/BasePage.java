package demoProject.WebAndMobDemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class BasePage {

	public static Properties propertiesFile;
	public static WebDriver driver;
	
	public void initializeWebDriver() throws IOException {
		
		//Loading the properties file
		Properties propertiesFile = new Properties();
		FileInputStream file = new FileInputStream(System.getProperty("user.dir")
				+ "\\src\\main\\java\\resources\\globalData.properties");
		propertiesFile.load(file);
		
		//Setting up the browser
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\src\\main\\java\\resources\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(propertiesFile.getProperty("url"));
		
	}
	
	public static void getScreenshot(String testName) throws IOException
	{
	File scrfile=	((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(scrfile,new File(System.getProperty("user.dir")+"\\Reports"+testName+".png"));
	
	}

}



