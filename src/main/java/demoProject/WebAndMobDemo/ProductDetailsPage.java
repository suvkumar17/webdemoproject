package demoProject.WebAndMobDemo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductDetailsPage {

	public ProductDetailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(//a[@title='Faded Short Sleeve T-shirts'])[2]")
	public WebElement prodTitle;

	@FindBy(xpath = "(//span[@itemprop='price'])[1]")
	public WebElement itemPrice;

	@FindBy(xpath = "//span[contains(.,'More')]")
	public WebElement moreBtn;

	@FindBy(xpath = "//span[@id='our_price_display']")
	public WebElement cartPrice;
	
	@FindBy(xpath = "//a[@title='View my shopping cart']")
	public WebElement shoppingCart;
	
	@FindBy(xpath = "//a[@title='View my shopping cart']")
	public WebElement emptyCart;
	

}
