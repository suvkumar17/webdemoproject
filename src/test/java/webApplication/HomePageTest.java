package webApplication;

import org.testng.annotations.Test;
import demoProject.WebAndMobDemo.BasePage;
import demoProject.WebAndMobDemo.HeaderPage;
import demoProject.WebAndMobDemo.HomePage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;

import static org.testng.Assert.assertEquals;
import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class HomePageTest extends BasePage{
	public static Logger log =LogManager.getLogger(HomePageTest.class.getName());
//	public static WebDriver driver;
	
	@BeforeTest
	public void initDriver() throws IOException {
	//Initializing the browser
		initializeWebDriver();
		log.info("Driver initialized");
	}

	@Test(priority = 1)
	public void validateTitle() {
	//	driver.get("http://automationpractice.com");
		String pageTitle = driver.getTitle();
		System.out.println(pageTitle);

		String expectedTitle = "Faded Short Sleeve T-shirts - My Store";

		//Comparing the page title to confirm we are in the correct page
		assertEquals(pageTitle, expectedTitle);
		log.info("Validated title");
	}

	@Test(priority = 2)
	public void searhProduct() throws FindFailed, InterruptedException {
		
		//Enter item name and search for the product
		HomePage homePage = new HomePage(driver);
		homePage.searhBox.clear();
		homePage.searhBox.sendKeys("t-shirt");
	//	homePage.searhBox.sendKeys(Keys.ENTER);
	//	log.info("Search Successfull");
	
		//Using Sikuli to click on search image
		Thread.sleep(1000);
/*		String filepath = "C://softwares//";
		Screen s = new Screen();
		Pattern openButton = new Pattern(filepath + "search.PNG");
		s.click(openButton);
*/
		HeaderPage headerPage = new HeaderPage(driver);
		headerPage.clickContactLink();
		Thread.sleep(1000);
	//	String st = "http://automationpractice.com/index.php?controller=contact";
		String contactURL = BasePage.propertiesFile.getProperty("contactUsPageURL");
		log.info(contactURL);
		assertEquals(headerPage.getContactUsURL(), contactURL);
		log.info("both expect and actual contactUSPage urls are same");
		
	//	System.out.println(headerPage.getContactUsURL());
		
	}
	
	
	
}
