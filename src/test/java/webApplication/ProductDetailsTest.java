package webApplication;

import org.testng.annotations.Test;

import demoProject.WebAndMobDemo.BasePage;
import demoProject.WebAndMobDemo.ProductDetailsPage;

import static org.testng.Assert.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class ProductDetailsTest extends BasePage {
	public static Logger log =LogManager.getLogger(ProductDetailsTest.class.getName());

	@Test
	public void comparePrice() throws InterruptedException {
		
		Thread.sleep(3000);
		ProductDetailsPage prod = new ProductDetailsPage(driver);
		//Check if the cart is empty
		String cartText = prod.emptyCart.getText();
		String cartTextExpected = "Cart (empty)";
		assertEquals(cartText, cartTextExpected);
		log.info("cart is empty");
		
		Thread.sleep(3000);
		//Hover to see the more button
		Actions actions = new Actions(driver);
		WebElement menuOption = prod.prodTitle;
		actions.moveToElement(menuOption).perform();
		
		//Get the product price and Click on the more button
		String priceInDetailsPage = prod.itemPrice.getText();
		prod.moreBtn.click();

		//Get the price in the basket
		String priceInBasket = prod.cartPrice.getText();
		
		
		//Compare the price in the details page with cart
		assertEquals(priceInDetailsPage, priceInBasket);
		log.info("price matches");
		
		}
	
	@AfterTest
	public void afterTest() {
		//Close the browser
	//	driver.close();
		System.out.println("Exiting");
		System.out.println("checking diff in git");

		
	}

}
